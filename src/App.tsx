import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import 'antd/dist/antd.css';
import { observer } from 'mobx-react-lite';

import './App.css';
import authStore from "./stores/authStore";
import AuthRouter from "./routers/AuthRouter";
import AppRouter from "./routers/AppRouter";

function App() {
  return (
      <Router>
        <Switch>
          <Route path={'/'} exact>
            <Redirect to="/login"/>
          </Route>

          <Route path={['/login', '/register']} exact>
            {
              authStore.token
                  ? <Redirect to="/main"/>
                  : <AuthRouter/>
            }
          </Route>


          <Route path={['/']}>
            <Redirect to="/login"/>
          </Route>


        </Switch>
      </Router>
  );
}

export default observer(App);

import ax, {AxiosResponse} from "axios";

let baseURL = process.env.BASE_URL || 'http://18.191.81.113:3001/api';


const baseAPI = ax.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
    }
});

export const authorizedAPI = ax.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
    }
});


//Without token
export const login = (data: LoginData):Promise<AxiosResponse<string>> => baseAPI.post('/login', data);
export const register = (data: LoginData):Promise<AxiosResponse<string>> => baseAPI.post('/register', data);


//With token
export const someMethod = ():Promise<AxiosResponse<any>> => authorizedAPI.get('/someMethod');
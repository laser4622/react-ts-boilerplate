import React from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import {UserOutlined, LockOutlined, MailOutlined} from '@ant-design/icons';
import {Link} from "react-router-dom";

import './Auth.css';
import authStore from "../../stores/authStore";


const LoginPage = () => {
    const onFinish = (values: LoginData) => {
        return authStore.login(values)
    };

    return (
        <div>
            <Form
                name="login"
                className="auth-form"
                onFinish={onFinish}
            >
                <Form.Item noStyle>
                    <h1 className="auth-form__label">Log in</h1>
                </Form.Item>
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Please input your Email!' }, {type: 'email', message: 'Please input valid Email'}]}
                >
                    <Input
                        prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your Password!' }]}
                >
                    <Input.Password
                        prefix={<LockOutlined/>}
                        placeholder="Password"
                    />
                </Form.Item>


                <Form.Item>
                    <Button type="primary" htmlType="submit" className="auth-form__button">
                        Log in
                    </Button>

                    Or <Link to="/register">register now!</Link>
                </Form.Item>
            </Form>

        </div>
    );
};

export default LoginPage;
import React from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { MailOutlined, LockOutlined } from '@ant-design/icons';
import authStore from "../../stores/authStore";
import {Link} from "react-router-dom";


const RegisterPage = () => {
    const onFinish = ({password2, ...data}: {email: string, password: string,  password2: string}) => {
        return authStore.register(data)
    };

    return (
        <div>
            <Form
                name="registration"
                className="auth-form"
                onFinish={onFinish}
            >
                <Form.Item noStyle>
                    <h1 className="auth-form__label">Registration</h1>
                </Form.Item>
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Please input your Email!' }, {type: 'email', message: 'Please input valid Email'}]}
                >
                    <Input
                        prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                </Form.Item>

                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your Password!' }]}
                >
                    <Input.Password
                        visibilityToggle={true}
                        prefix={<LockOutlined />}
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item
                    name="password2"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                        }),
                    ]}
                >
                    <Input.Password
                        prefix={<LockOutlined />}
                        placeholder="Confirm Password"
                    />
                </Form.Item>


                <Form.Item>
                    <Button type="primary" htmlType="submit" className="auth-form__button">
                        Register
                    </Button>

                    Or <Link to="/login">log in!</Link>
                </Form.Item>
            </Form>

        </div>
    );
};

export default RegisterPage;
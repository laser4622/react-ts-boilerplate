import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import LoginPage from "../containers/Auth/LoginPage";
import RegisterPage from "../containers/Auth/RegisterPage";


const AuthRouter = () => {
    return (
        <Router>
            <Switch>
                lol
                <Route path='/login' exact><LoginPage/></Route>
                <Route path='/register' exact><RegisterPage/></Route>
            </Switch>
        </Router>
    )
};

export default AuthRouter;
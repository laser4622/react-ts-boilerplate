import {action, makeAutoObservable, observable} from "mobx";
import {notification} from "antd";
import Cookie from "js-cookie"
import {AxiosRequestConfig} from "axios";

import * as api from "../api";

class AuthStore {
    @observable
    token: string | null

    constructor() {
        makeAutoObservable(this)
        this.token = Cookie.get('token') || null
    }

    @action
    async login(data: LoginData) {
        try {
            const resp = await api.login(data)
            this.token = resp.data || null
            Cookie.set("token", this.token || "");
        } catch (e) {
            notification.error({message: e.response?.data[0]?.message || e.message ||'Unknown error'})
            console.error(e.response)
        }
    }

    @action
    async register(registerData: RegistrationData) {
        try {
            const r = await api.register(registerData)
            this.token = r.data || null

            Cookie.set("token", this.token || "");
        } catch (e) {
            notification.error({message: e.response?.data[0]?.message || e.message ||'Unknown error'})
            console.error(e.response)
        }
    }

    @action
    async logout(){
        this.token = null;
        Cookie.remove('token')
    }

}

const authStore = new AuthStore();

api.authorizedAPI.interceptors.request.use((config: AxiosRequestConfig) => {
    config.headers[ "Authorization"] = `Token ${authStore.token}`;
    return config;
});

api.authorizedAPI.interceptors.response.use(undefined, async (error: any) => {
    if (error.response && error.response.status === 401) {
        await authStore.logout()
    }
    return Promise.reject(error);
});


export default authStore